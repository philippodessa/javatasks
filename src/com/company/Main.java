package com.company;

import java.util.ArrayList;
import java.util.Arrays;

// Домашнее задание №2 c прочтением текстового файла и HashMap
import java.util.*;
import java.io.IOException;
import java.nio.file.*;

public class Main {


    // Домашнее задание №1 c пирамидкой------------------------------------------------------------------------------------------

    public static void printRightPart(int highLevel) {

        String tags = "##";

        int some = highLevel - 1;


        for (int n = 0; n < highLevel; n++) {

            ArrayList<String> emptySymbols = new ArrayList<>();

            for (int i = 0; i < some; i++) {
                emptySymbols.add("_");
            }

            String dirtyString = Arrays.toString(emptySymbols.toArray());
            String netString = dirtyString.replaceAll("[ , \\[\\]]", "");


            System.out.println(tags + netString);

            some = some - 1;
            tags = tags + "#";
        }
    }


    public static void printLeftPart(int highLevel) {

        String tags = "##";

        int some = highLevel - 1;


        for (int n = 0; n < highLevel; n++) {

            ArrayList<String> emptySymbols = new ArrayList<>();

            for (int i = 0; i < some; i++) {
                emptySymbols.add("_");
            }

            String dirtyString = Arrays.toString(emptySymbols.toArray());
            String netString = dirtyString.replaceAll("[ , \\[\\]]", "");


            System.out.println(netString + tags);

            some = some - 1;
            tags = tags + "#";
        }
    }


    public static void printBoth(int highLevel) {

        String tags = "##";

        int some = highLevel - 1;


        for (int n = 0; n < highLevel; n++) {

            ArrayList<String> emptySymbols = new ArrayList<>();

            for (int i = 0; i < some; i++) {
                emptySymbols.add("_");
            }

            String dirtyString = Arrays.toString(emptySymbols.toArray());
            String netString = dirtyString.replaceAll("[ , \\[\\]]", "");


            System.out.println(netString + tags + "  " + tags + netString);

            some = some - 1;
            tags = tags + "#";
        }
    }



    public static String returnDomain (String url) {
        String domain = url.substring(url.lastIndexOf(".") + 1);
        if (domain.contains("/")){
            domain = domain.substring(0 , domain.indexOf("/"));
        }
        return domain;
    }



    // Домашнее задание №2 c прочтением текстового файла и HashMap------------------------------------------------------------------------------------------

    // method for reading file
    public static String readFilesAsString (String fileName) throws IOException {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data;
    }

    // method for getting clear words from our file without  punctuation
    public static ArrayList<String> words (String text) throws IOException {
        String punctuations = "~`!@#$%^&*()_-+=[]{} \\ | ;: \' \" ,.<>/? 1234567890";
        List<String> wordsInLine = new ArrayList<>(Arrays.asList(readFilesAsString(text).toLowerCase().split("\\s+")));
        ArrayList<String> clearWords = new ArrayList<>();

        for (int words=0; words < wordsInLine.size(); words++){
            int startIndex = 0;
            int endIndex = 0;

            for (int chars= 0; chars < wordsInLine.get(words).length(); chars++){
                if ( punctuations.indexOf(wordsInLine.get(words).charAt(chars))>=0) {
                    startIndex = startIndex+1;
                }
                else break;
            }

            for (int chars= wordsInLine.get(words).length()-1; chars >0; chars--){
                if ( punctuations.indexOf(wordsInLine.get(words).charAt(chars))>=0) {
                    endIndex = endIndex+1;
                }
                else break;
            }

            String newWord = wordsInLine.get(words).substring(startIndex, wordsInLine.get(words).length() - endIndex);
            clearWords.add(newWord);
        }
        return clearWords;
    }


    // method return for printing frequency of words in line
    static HashMap<String, Integer>  frequencyOfWords1 (ArrayList<String> listOfWords){
        HashMap<String, Integer> wordCount = new HashMap<String, Integer>();

        for(String word: listOfWords ) {
            Integer count = wordCount.get(word);
            wordCount.put(word, (count==null) ? 1 : count+1);
        }

        return wordCount;
    }


    // method return for printing frequency in order by values

    public static void  sortByValue (HashMap <String,Integer> unsortMap) {
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        Map<String,Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry: list){
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        for (Map.Entry<String, Integer> entry : sortedMap.entrySet()){
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }






    public static void main(String[] args) throws Exception {
       //printRightPart(5);
       //printLeftPart(5);
       //printBoth(5);


        // Домашнее задание №1 c пирамидкой

       /* System.out.println(returnDomain("https://www.domain.com/domains/"));
        System.out.println(returnDomain("https://www.domain.com/domains/somethingelse"));
        System.out.println(returnDomain("https://en.wikipedia.org/wiki/.org/internet"));
        System.out.println(returnDomain("https://www.californiacolleges.edu/#/"));
        System.out.println(returnDomain("https://www.segodnya.ua/"));
       */

        // Домашнее задание №2 c прочтением текстового файла и HashMap

        System.out.println("Total amount of Words in textfile is:  " + words("C:\\Users\\E6440\\Desktop\\Text for homework.txt").size());
        // print sorted list of words frequency
        sortByValue(frequencyOfWords1(words("C:\\Users\\E6440\\Desktop\\Text for homework.txt")));

    }
}
